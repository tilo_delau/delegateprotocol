//
//  ViewController.swift
//  DelegateProtocol
//
//  Created by Tilo on 2021-07-12.
//

import UIKit

class ReceiverVC: UIViewController {

    
    // delegate
    @IBOutlet weak var lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnToSenderVC(_ sender: Any) {
        
        let SenderVC = storyboard?.instantiateViewController(withIdentifier: "SenderVC") as! SenderVC
        SenderVC.selectionDelegate = self
        present(SenderVC, animated: true, completion: nil)
     }
    
}

extension ReceiverVC: selectDelgate {
    
    func select(s: String) {
        lbl.text = s
    }
    
}

