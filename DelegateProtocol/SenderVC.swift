//
//  SenderVC.swift
//  DelegateProtocol
//
//  Created by Tilo on 2021-07-12.
//
//
// 1. Create the protocol
// 2. SenderVC: Declare the protocal as a weak var and call it
// 3. ReceiverVC: Implment the protocol, method and method
//

import UIKit

protocol selectDelgate: class {
    
    func select(s: String)
    
}

class SenderVC: UIViewController {

    
    weak var selectionDelegate: selectDelgate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btn1(_ sender: Any) {
        selectionDelegate.select(s: "One")
        dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func btn2(_ sender: Any) {
        selectionDelegate.select(s: "Two")
        dismiss(animated: true, completion: nil)
        
    }
    
}
